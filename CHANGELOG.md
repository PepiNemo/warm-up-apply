## 1.0.0 (2023-02-24)

* Backup thinkpad ([e764dd2](https://gitlab.com/PepiNemo/warm-up-apply/commit/e764dd2))
* feat, refactor(Backend):  Allow to validate and type the data to be saved. refactor cronjob ([078213d](https://gitlab.com/PepiNemo/warm-up-apply/commit/078213d))
* feat(Front, Back, Mongo): The frontend backend was initialized and docker-compose mongo already work ([40537c4](https://gitlab.com/PepiNemo/warm-up-apply/commit/40537c4))
* fix(Cron, PostList): Cron no longer throws key duplicate error and Postlist now it no longer shows d ([2164c71](https://gitlab.com/PepiNemo/warm-up-apply/commit/2164c71))
* Fix(Front): Run in production mode and dynamic routes are now rendered dynamically on the server sid ([fc5dbe7](https://gitlab.com/PepiNemo/warm-up-apply/commit/fc5dbe7))
* refactor(Frontend): The components were factored with the atomic design pattern ([5b92a9d](https://gitlab.com/PepiNemo/warm-up-apply/commit/5b92a9d))
* feat(Back-Dockerfile):  Add the posibility of raising the backend as production ([7f13fcf](https://gitlab.com/PepiNemo/warm-up-apply/commit/7f13fcf))
* feat(README): Added a Readme to the project. ([3127c79](https://gitlab.com/PepiNemo/warm-up-apply/commit/3127c79))
* fix(environment variables): Added  sample environment to run the sample project ([b7fa124](https://gitlab.com/PepiNemo/warm-up-apply/commit/b7fa124))
* fix(Post): Redirect to url or story_url of a post. ([63890f1](https://gitlab.com/PepiNemo/warm-up-apply/commit/63890f1))



