# Warm-up Apply School

This is a practice project for the NEST and Next JS frameworks. The project consists of a Hacker New post view extracted by the backend from the algolia api stored in mongodb and queried and displayed on the Frontend.

## Requirements

1. Docker installed.
2. Docker comppose installed.

## Installation from git

```
git clone https://gitlab.com/PepiNemo/warm-up-apply

cd warm-up-apply

docker compose up --build
```
