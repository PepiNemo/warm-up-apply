import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { PostModule } from "./post/post.module";
import { ScheduleModule } from "@nestjs/schedule";
import { CronModule } from "./post/cronjob/cron.module";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: ".env.example" }),
    PostModule,
    CronModule,
    MongooseModule.forRoot(process.env.DB_URL),
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
