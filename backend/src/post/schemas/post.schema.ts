import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument, Types } from "mongoose";

export type PostDocument = HydratedDocument<PostSchemaClass>;

@Schema()
export class PostSchemaClass {
  _id: Types.ObjectId;

  @Prop({ unique: true })
  objectID: string;

  @Prop()
  created_at: Date;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: string;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: string;

  @Prop()
  created_at_i: number;

  @Prop()
  _tags: string[];
}

export const PostSchema = SchemaFactory.createForClass(PostSchemaClass);
