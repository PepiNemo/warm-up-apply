import {
  IsArray,
  IsDateString,
  IsFQDN,
  IsNotEmpty,
  IsNumber,
  IsString,
} from "class-validator";

export class CreatePostDto {
  @IsString()
  @IsNotEmpty()
  objectID: string;

  @IsDateString()
  created_at: Date;

  @IsString()
  title: string;

  @IsFQDN()
  url: string;

  @IsString()
  author: string;

  @IsNumber()
  points: number;

  @IsString()
  story_text: string;

  @IsString()
  comment_text: string;

  @IsNumber()
  num_comments: number;

  @IsString()
  story_id: string;

  @IsString()
  story_title: string;

  @IsString()
  story_url: string;

  @IsString()
  parent_id: string;

  @IsNumber()
  created_at_i: number;

  @IsArray()
  _tags: string[];
}
