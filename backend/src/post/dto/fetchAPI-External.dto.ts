import { IsBoolean, IsNumber, IsString } from "class-validator";
import { CreatePostDto } from "./create-post.dto";
import { Exhaustive, ProcessingTimingsMS } from "./dto.interfaces";

export class FetchAPIExternal {
  hits: CreatePostDto[];

  @IsNumber()
  nbHits: number;

  @IsNumber()
  page: number;

  @IsNumber()
  nbPages: number;

  @IsNumber()
  hitsPerPage: number;

  @IsBoolean()
  exhaustiveNbHits: boolean;

  @IsBoolean()
  exhaustiveTypo: boolean;

  exhaustive: Exhaustive;

  @IsString()
  query: string;

  @IsString()
  params: string;

  @IsNumber()
  processingTimeMS: number;

  processingTimingsMS: ProcessingTimingsMS;

  @IsNumber()
  serverTimeMS: number;
}
