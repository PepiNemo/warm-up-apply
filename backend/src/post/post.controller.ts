import { Controller, Get, Param, Delete } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PostService } from "./post.service";

@ApiTags("post")
@Controller("post")
export class PostController {
  constructor(private readonly postService: PostService) {}
  @Get()
  findAll() {
    return this.postService.findAll();
  }

  @Get("OnePage/:page")
  findOnePage(@Param("page") page: string) {
    return this.postService.findOnePage(+page);
  }

  @Delete("/SoftDelete/:id")
  softDelete(@Param("id") id: string) {
    return this.postService.softDelete(id);
  }
}
