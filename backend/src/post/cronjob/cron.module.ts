import { Module } from "@nestjs/common";
import { TasksService } from "./cron.services";

import { RepositoryModule } from "src/post/repository/repository.module";
import { ProviderModule } from "../provider/provider.module";

@Module({
  imports: [RepositoryModule, ProviderModule],
  providers: [TasksService],
})
export class CronModule {}
