import { Inject, Injectable, Logger } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { CreatePostDto } from "src/post/dto/create-post.dto";
import { RepositoryService } from "src/post/repository/repository.service";
import { ProviderService } from "../provider/provider.service";

@Injectable()
export class TasksService {
  constructor(
    @Inject(RepositoryService)
    private readonly repositoryService: RepositoryService,
    @Inject(ProviderService)
    private readonly providerService: ProviderService
  ) {}

  private readonly logger = new Logger(TasksService.name);

  @Cron(CronExpression.EVERY_MINUTE)
  async handleCron() {
    this.logger.debug("Llamando cada 1 minuto");
    const { nbPages }: { nbPages: number } =
      await this.providerService.getExternalPage(0);

    for (let i = 0; i < nbPages; i++) {
      const { hits }: { hits: CreatePostDto[] } =
        await this.providerService.getExternalPage(i);

      await Promise.all(
        hits.map((hit: CreatePostDto) => {
          return this.repositoryService.updateOrCreate(hit);
        })
      );
    }
  }
}
