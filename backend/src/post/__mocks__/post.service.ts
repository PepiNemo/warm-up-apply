import { postExample } from "./exampleData/post.example";

export const PostService = jest.fn().mockReturnValue({
  findAll: jest.fn().mockResolvedValue([postExample()]),
  findOnePage: jest.fn().mockResolvedValue([postExample()]),
  softDelete: jest.fn().mockResolvedValue(postExample()),
});
