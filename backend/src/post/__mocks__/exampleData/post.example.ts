import { PostSchemaClass } from "src/post/schemas/post.schema";
import { Types } from "mongoose";

export const postExample = (): PostSchemaClass => {
  return {
    _id: new Types.ObjectId("63e689e1b441a3693c12dad7"),
    objectID: "34743094",
    created_at: new Date("2023-02-10T17:53:28.000Z"),
    title: null,
    url: null,
    author: "zubairq",
    points: null,
    story_text: null,
    comment_text:
      "Correct. 95% of the work on a crypto project is just HTML, CSS, Javascript, and maybe NodeJS",
    num_comments: null,
    story_id: "34741961",
    story_title: "Most Crypto Developers Aren't Working on Bitcoin or Ethereum",
    story_url:
      "https://somereverie.substack.com/p/most-crypto-developers-arent-working",
    parent_id: "34741961",
    created_at_i: 1676051608,
    _tags: ["comment", "author_zubairq", "story_34741961"],
  };
};
