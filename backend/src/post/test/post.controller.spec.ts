import { Test, TestingModule } from "@nestjs/testing";
import { PostController } from "../post.controller";
import { PostService } from "../post.service";
import { postExample } from "../__mocks__/exampleData/post.example";

jest.mock("../post.service");

describe("PostController", () => {
  let postController: PostController;
  let postService: PostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostController],
      providers: [PostService],
    }).compile();

    postController = module.get<PostController>(PostController);
    postService = module.get<PostService>(PostService);
  });

  it("should be defined", () => {
    expect(postController).toBeDefined();
  });

  describe("findAll", () => {
    describe("when findAll is called", () => {
      let posts;
      beforeEach(async () => {
        posts = await postService.findAll();
      });

      test("then it should call postService.findAll with no params", () => {
        expect(postService.findAll).toBeCalled();
      });

      test("then it should return posts", () => {
        expect(posts).toEqual([postExample()]);
      });
    });
  });

  describe("findOnePage", () => {
    describe("when findOnePage is called", () => {
      let onePage;
      beforeEach(async () => {
        onePage = await postService.findOnePage(3);
      });

      test("then it should call postService.findOnePage with single param", () => {
        expect(postService.findOnePage).toBeCalledWith(3);
      });

      test("the it should return onePage ", () => {
        expect(onePage).toEqual([postExample()]);
      });
    });
  });

  describe("softDelete", () => {
    describe("when softDelete is called ", () => {
      let softDeletePost;

      beforeEach(async () => {
        softDeletePost = await postService.softDelete(`${postExample()._id}`);
      });

      test("then it should call softdelete with single param", () => {
        expect(postService.softDelete).toBeCalledWith(`${postExample()._id}`);
      });

      test("then it should return a post", () => {
        expect(softDeletePost).toEqual(postExample());
      });
    });
  });
});
