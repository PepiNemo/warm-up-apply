import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from "@nestjs/common";

import { RepositoryService } from "./repository/repository.service";
import { PostSchemaClass } from "./schemas/post.schema";
import { isValidObjectId } from "mongoose";

@Injectable()
export class PostService {
  constructor(
    @Inject(RepositoryService)
    private readonly repositoryService: RepositoryService
  ) {}

  async findAll() {
    try {
      return await this.repositoryService.getAll();
    } catch (error) {
      throw new HttpException(
        { status: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message },
        HttpStatus.INTERNAL_SERVER_ERROR,
        { cause: error }
      );
    }
  }

  async findOnePage(page: number) {
    try {
      return await this.repositoryService.getOnePage(page);
    } catch (error) {
      throw new HttpException(
        { status: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message },
        HttpStatus.INTERNAL_SERVER_ERROR,
        { cause: error }
      );
    }
  }

  async softDelete(id: string) {
    if (isValidObjectId(id)) {
      let post: PostSchemaClass;
      try {
        post = await this.repositoryService.findByIdAndUpdate(id);
      } catch (error) {
        throw new HttpException(
          { status: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message },
          HttpStatus.INTERNAL_SERVER_ERROR,
          { cause: error }
        );
      }
      if (post != undefined) {
        return post;
      } else {
        throw new NotFoundException();
      }
    } else {
      throw new BadRequestException();
    }
  }
}
