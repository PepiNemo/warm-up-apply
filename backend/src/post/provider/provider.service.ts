import { Injectable } from "@nestjs/common";
import { FetchAPIExternal } from "../dto/fetchAPI-External.dto";

@Injectable()
export class ProviderService {
  async getExternalPage(page: number): Promise<FetchAPIExternal> {
    return fetch(`${process.env.URL_EXTERNAL_API}&page=${page}`).then((res) =>
      res.json()
    );
  }
}
