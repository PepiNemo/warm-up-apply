import { Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { PostSchemaClass } from "../schemas/post.schema";
import type { PostDocument } from "../schemas/post.schema";
import { CreatePostDto } from "../dto/create-post.dto";

@Injectable()
export class RepositoryService {
  constructor(
    @InjectModel(PostSchemaClass.name) private PostModel: Model<PostDocument>
  ) {}

  getAll(): Promise<PostDocument[]> {
    return this.PostModel.find().exec();
  }

  getOnePage(page: number): Promise<PostDocument[]> {
    const offset = 30 * page;
    return this.PostModel.find()
      .sort({ created_at: -1 })
      .skip(offset)
      .limit(30)
      .where("create_at_i")
      .gt(0)
      .exec();
  }

  findByIdAndUpdate(id: string) {
    return this.PostModel.findByIdAndUpdate(
      id,
      { created_at_i: -1 },
      { returnDocument: "after" }
    );
  }

  async updateOrCreate(hit: CreatePostDto) {
    if (
      (hit.title == null && hit.story_id == null) ||
      (hit.url == null && hit.story_url == null)
    ) {
      return null;
    }

    const post = await this.PostModel.findOne({ objectID: hit.objectID });
    if (!post) {
      return this.PostModel.create(hit);
    }

    delete hit.created_at_i;
    return this.PostModel.findOneAndUpdate({ objectID: hit.objectID }, hit);
  }
}
