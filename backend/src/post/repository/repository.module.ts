import { Module } from "@nestjs/common";
import { RepositoryService } from "./repository.service";
import { MongooseModule } from "@nestjs/mongoose";
import { PostSchema, PostSchemaClass } from "../schemas/post.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PostSchemaClass.name, schema: PostSchema },
    ]),
  ],
  providers: [RepositoryService],
  exports: [
    RepositoryService,
    MongooseModule.forFeature([
      { name: PostSchemaClass.name, schema: PostSchema },
    ]),
  ],
})
export class RepositoryModule {}
