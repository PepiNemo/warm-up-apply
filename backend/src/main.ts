import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: [process.env.FRONTEND_URL, "http://localhost:3001"],
    methods: "GET,DELETE",
  });
  app.useGlobalPipes(new ValidationPipe({ skipNullProperties: true }));
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
