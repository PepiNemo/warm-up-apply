import HomePage from "../components/templates/homePage/homePage";

export const dynamic = "force-dynamic",
  revalidate = 0;

export default async function PostPage({
  params,
}: {
  params: { actualIndexPage: number };
}) {
  const actualIndexPage = +params.actualIndexPage || 1;

  const PostPage: JSON[] = await fetch(
    `${process.env.SERVER_URL_BACKEND}/OnePage/${actualIndexPage}`,
    { cache: "no-store" }
  ).then((res) => res.json());
  const validIndexPage = PostPage.length > 0 ? actualIndexPage : 1;

  return (
    <>
      {/* @ts-expect-error Server Component */}
      <HomePage actualIndexPage={validIndexPage} />
    </>
  );
}
