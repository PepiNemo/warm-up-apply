import { Alef } from "@next/font/google";

const alef = Alef({
  weight: "400",
  style: ["normal"],
  subsets: ["latin"],
});

export default alef;
