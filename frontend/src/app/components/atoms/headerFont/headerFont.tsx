import { Rock_Salt } from "@next/font/google";

const headerFont = Rock_Salt({
  weight: "400",
  style: ["normal"],
  subsets: ["latin"],
});

export default headerFont;
