"use client";

import Post from "../../molecules/post/post";
import styles from "./postList.module.css";
import Spinner from "../../atoms/spinner/spinner";

import { PostInterface } from "../../../types";
import { useEffect, useState } from "react";

const URL_SERVER = process.env.NEXT_PUBLIC_SERVER_URL_BACKEND;

export default function PostList({
  actualIndexPage = 0,
  Posts,
}: {
  actualIndexPage: number;
  Posts: any;
}) {
  const [posts, setPosts] = useState([]);
  const [firstRender, setFirstRender] = useState(true);
  const [waitingTimeOver, setWaitingTime] = useState(false);
  const [renderAgain, setRender] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setWaitingTime(true);
    }, 2000);

    if (firstRender == true) {
      setPosts(Posts);
    } else {
      fetch(`${URL_SERVER}/OnePage/${actualIndexPage}`, {
        mode: "cors",
        cache: "no-store",
      })
        .then((res) => res.json())
        .then((resPosts) => setPosts(resPosts));
    }

    setFirstRender(false);
  }, [renderAgain]);

  const softDelete = async (
    e: React.FormEvent<HTMLFormElement>,
    _id: string
  ) => {
    e.preventDefault();
    e.stopPropagation();
    await fetch(`${URL_SERVER}/SoftDelete/${_id}`, {
      method: "DELETE",
      mode: "cors",
    }).then((res) => res.json());
    setRender(!renderAgain);
    setPosts([]);
    setWaitingTime(false);
  };

  if (posts.length == 0) {
    if (waitingTimeOver == false) return null;
    if (waitingTimeOver == true)
      return <Spinner message="Loading Posts ...." />;
  }

  return (
    <div className={styles.postList}>
      {posts.map((post: PostInterface) => {
        return post.created_at_i > 0 ? (
          <Post key={post.objectID} softDelete={softDelete} {...post} />
        ) : null;
      })}
    </div>
  );
}
