import styles from "./footer.module.css"

export default function Footer({
    children,
  }: {
    children: React.ReactNode;
  }){
    return <div className={styles.Footer}>
        {children}
    </div>
}