import { Suspense } from "react";
import PostList from "../../organisms/postList/postList";
import Pagination from "../../molecules/pagination/pagination";
import Footer from "../../organisms/footer/footer";
import Spinner from "../../atoms/spinner/spinner";
import { getActualPagePost, validIndexPages } from "@/app/utils/Posts";
import pageFont from "../../atoms/pageFont/pageFont";
import Header from "../../organisms/header/header";
import HeaderTitles from "../../molecules/headerTitles/headerTitles";

export default async function HomePage({
  actualIndexPage = 1,
}: {
  actualIndexPage: number;
}) {
  const actualPagePosts = await getActualPagePost(actualIndexPage);
  const IndexPages = await validIndexPages(actualIndexPage);

  return (
    <div className={pageFont.className}>
      <Header>
        <HeaderTitles Title="HN FEED" span="WE hacker news!" />
      </Header>

      <PostList actualIndexPage={actualIndexPage} Posts={actualPagePosts} />

      <Suspense fallback={<Spinner message="Loading index valid Pages ..." />}>
        <Footer>
          {/* @ts-expect-error Server Component */}
          <Pagination page={actualIndexPage} IndexPages={IndexPages} />
        </Footer>
      </Suspense>
    </div>
  );
}
