import Link from "next/link";
import styles from "./pagination.module.css";

export default async function Pagination({
  page = 1,
  IndexPages,
}: {
  page: number;
  IndexPages: number[];
}) {
  return (
    <ul className={styles.Index}>
      {IndexPages.map((NumPage) =>
        NumPage == page ? (
          <Link
            key={NumPage}
            className={styles.ActualLink}
            href={`/${NumPage}`}
          >
            {NumPage}
          </Link>
        ) : (
          <Link key={NumPage} className={styles.Link} href={`/${NumPage}`}>
            {NumPage}
          </Link>
        )
      )}
    </ul>
  );
}
