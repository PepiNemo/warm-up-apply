import { getDate } from "../../../utils/dayjs";
import styles from "./post.module.css";

export default function Post({
  _id,
  title,
  story_title,
  author,
  created_at,
  softDelete,
  story_url,
  url,
}: {
  _id: string;
  title: string;
  story_title: string;
  author: string;
  created_at: string;
  softDelete: Function;
  story_url: string;
  url: string;
}) {
  const Hours: string | number = getDate(created_at);

  return (
    <div className={styles.Post}>
      <article
        className={styles.Article}
        onClick={() => {
          location.href = url || story_url;
        }}
      >
        <h2>{title || story_title} </h2>
        <span>{` - ${author}` || " - author"}</span>
        <div className={styles.Hours}>{Hours}</div>
        <button
          className={styles.SoftDeleteButton}
          onClick={(e) => softDelete(e, _id)}
        >
          🗑
        </button>
      </article>
      <div className={styles.borderBotton}></div>
    </div>
  );
}
