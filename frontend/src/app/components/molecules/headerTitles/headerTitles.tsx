import styles from "./headerTitles.module.css";
import headerFont from "../../atoms/headerFont/headerFont";

export default function HeaderTitles({
  Title,
  span,
}: {
  Title: string;
  span: string;
}) {
  return (
    <div className={headerFont.className}>
      <h1 className={styles.bigTitle}>{Title}</h1>
      <span className={styles.smallTitle}>{span}</span>
    </div>
  );
}
