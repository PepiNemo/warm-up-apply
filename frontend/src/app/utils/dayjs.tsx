import relativeTime from "dayjs/plugin/relativeTime";
import updateLocale from "dayjs/plugin/updateLocale";
import dayjs from "dayjs";

export function getDate(created_at: string): string | number {
  dayjs.extend(relativeTime);
  dayjs.extend(updateLocale);
  dayjs.updateLocale("en", {
    relativeTime: {
      future: "%s",
      past: "%s",
      s: "Today",
      m: "Today",
      mm: "Today",
      h: "Today",
      hh: "Today",
      d: "Yestarday",
      dd: "%d ",
      M: "%d ",
      MM: "%d",
      y: "%d",
      yy: "%d",
    },
  });

  const PostDate = dayjs(created_at);
  const gapDate = dayjs(PostDate).fromNow();
  const Date = dayjs(PostDate).format("DD MMM");
  return isNaN(gapDate as any) ? gapDate : Date;
}
