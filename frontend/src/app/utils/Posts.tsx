export const getActualPagePost = (actualIndexPage: number) => {
  return fetch(
    `${process.env.SERVER_URL_BACKEND}/OnePage/${actualIndexPage - 1}`,
    {
      mode: "cors",
      cache: "no-store",
    }
  ).then((res) => res.json());
};

export const validIndexPages = async (page: number) => {
  const index: number[] = [];
  let offsets: number[] = [];
  if (typeof page != "number") {
    return [1, 2, 3, 4, 5, 6, 7];
  }
  if (page > 4) {
    index.push(page - 3, page - 2, page - 1, page);
    offsets = [1, 2, 3];
  } else {
    offsets = [
      -page + 1,
      -page + 2,
      -page + 3,
      -page + 4,
      -page + 5,
      -page + 6,
      -page + 7,
    ];
  }
  for (const offset of offsets) {
    const PostPage: JSON[] = await fetch(
      `${process.env.SERVER_URL_BACKEND}/OnePage/${+page - 1 + offset}`,
      { mode: "cors", cache: "no-store" }
    ).then((res) => res.json());
    if (PostPage.length > 0) {
      index.push(+page + offset);
    }
  }

  return index;
};
