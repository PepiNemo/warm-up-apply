import HomePage from "./components/templates/homePage/homePage";

export default function Home() {
  return (
    <>
      {/* @ts-expect-error Server Component */}
      <HomePage actualIndexPage={1} />
    </>
  );
}
